const url = "https://treinamento-ajax-api.herokuapp.com/messages"

const updateMsg = () => {
  fetch(url)
  .then((response) => response.json())
  .then((messages) => {
    const body = document.querySelector('.chat')
    messages.map((item) => {
      console.log(item.content)
      const msg = document.createElement("div")
      msg.classList.add("msg")
      msg.setAttribute('id',item.id)
      msg.innerHTML = `<p>${item.content} </p>
          <div class="options">
            <button class="button edt" onclick="edita(this)">Editar</button>
            <button class="button exc" onclick="exclui(this)">Excluir</button>
          </div>`
      body.appendChild(msg)
    })
  })
  .catch((error) => {
    console.log(error)
  })
}
updateMsg()

const envia = () => {
  const txt = document.querySelector('.text-box').value
  const body = document.querySelector('.chat')
  const msg = document.createElement("div")
  msg.classList.add("msg")
  msg.innerHTML = `<p>${txt} </p>
          <div class="options">
            <button class="button edt" onclick="edita(this)">Editar</button>
            <button class="button exc" onclick="exclui(this)">Excluir</button>
          </div>
`
  body.appendChild(msg)
  postMessage(txt)
  document.querySelector('.text-box').value = ""
}

const exclui = (elemento) => {
  const id = '/' + elemento.parentElement.parentElement.id
  const config = {
    method: 'DELETE',
    headers: {
      "content-Type": "application/json"
    }

  }
  fetch(url + id, config)
  .then((response) => response.json())
  .then((messages) => {
    console.log(messages)
  })
  .catch((error) => {
    console.log(error)
  })
  elemento.parentElement.parentElement.remove()
}

const edita = (elemento) => {
  const msg = elemento.parentElement.parentElement.querySelector('p')
  const input = document.createElement('input')
  input.value = msg.innerHTML
  msg.parentNode.replaceChild(input, msg)
  elemento.setAttribute("onclick", "salva(this)")
  elemento.innerHTML = "salvar"
  elemento.style.background = "green"
}

const salva = (elemento) => {
  const input = elemento.parentElement.parentElement.querySelector('input')
  const id = '/' + elemento.parentElement.parentElement.id
  putMessage(input.value, id)
  const msg = document.createElement('p')
  msg.innerHTML = input.value
  input.parentNode.replaceChild(msg, input)
  elemento.setAttribute("onclick", "edita(this)")
  elemento.innerHTML = "Edita"
  elemento.style.background = "yellow"
  console.log("salvo")
}

const putMessage = (msg, id) => {
  const body = {
    message: {
      content: msg
    }
  }

  const config = {
    method: "PUT",
    body: JSON.stringify(body),
    headers: {
      "content-Type": "application/json"
    }
  }

  fetch(url + id, config)
  .then((response) => response.json())
  .then((messages) => {
    console.log(messages)
  })
  .catch((error) => {
    console.log(error)
  })
}

const postMessage = (msg) => {
  const body = {
    message: {
      content: msg,
      author: "Thiago Prata"
    }
  }

  const config = {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "content-Type": "application/json"
    }
  }

  fetch(url, config)
  .then((response) => response.json())
  .then((messages) => {
    console.log(messages)
  })
  .catch((error) => {
    console.log(error)
  })

}
